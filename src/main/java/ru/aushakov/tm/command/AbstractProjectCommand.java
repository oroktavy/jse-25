package ru.aushakov.tm.command;

import org.jetbrains.annotations.Nullable;
import ru.aushakov.tm.exception.entity.ProjectNotFoundException;
import ru.aushakov.tm.model.Project;

public abstract class AbstractProjectCommand extends AbstractCommand {

    protected void showProject(@Nullable final Project project) {
        if (project == null) throw new ProjectNotFoundException();
        System.out.println("ID: " + project.getId());
        System.out.println("NAME: " + project.getName());
        System.out.println("STATUS: " + project.getStatus().getDisplayName());
        System.out.println("DESCRIPTION: " + project.getDescription());
        System.out.println("CREATED: " + project.getCreated());
        System.out.println("START DATE: " + project.getStartDate());
        System.out.println("END DATE: " + project.getEndDate());
    }

}
