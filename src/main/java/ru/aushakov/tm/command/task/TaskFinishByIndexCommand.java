package ru.aushakov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.aushakov.tm.command.AbstractTaskCommand;
import ru.aushakov.tm.constant.TerminalConst;
import ru.aushakov.tm.enumerated.Role;
import ru.aushakov.tm.exception.entity.TaskNotFoundException;
import ru.aushakov.tm.util.TerminalUtil;

import java.util.Optional;

public final class TaskFinishByIndexCommand extends AbstractTaskCommand {

    @Override
    @NotNull
    public String getName() {
        return TerminalConst.TASK_FINISH_BY_INDEX;
    }

    @Override
    @Nullable
    public String getArgument() {
        return null;
    }

    @Override
    @NotNull
    public String getDescription() {
        return "Finish task by index";
    }

    @Override
    @NotNull
    public Role[] getRoles() {
        return new Role[]{Role.USER, Role.ADMIN};
    }

    @Override
    public void execute() {
        @NotNull final String userId = serviceLocator.getAuthService().getUserId();
        System.out.println("[FINISH TASK]");
        System.out.println("ENTER TASK INDEX:");
        @NotNull final Integer index = TerminalUtil.nextNumber() - 1;
        Optional.ofNullable(serviceLocator.getTaskService().finishOneByIndex(index, userId))
                .orElseThrow(TaskNotFoundException::new);
    }

}
