package ru.aushakov.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.aushakov.tm.command.AbstractProjectCommand;
import ru.aushakov.tm.constant.TerminalConst;
import ru.aushakov.tm.enumerated.Role;
import ru.aushakov.tm.exception.entity.ProjectNotFoundException;
import ru.aushakov.tm.util.TerminalUtil;

import java.util.Optional;

public final class ProjectRemoveByIdCommand extends AbstractProjectCommand {

    @Override
    @NotNull
    public String getName() {
        return TerminalConst.PROJECT_REMOVE_BY_ID;
    }

    @Override
    @Nullable
    public String getArgument() {
        return null;
    }

    @Override
    @NotNull
    public String getDescription() {
        return "Remove project by id";
    }

    @Override
    @NotNull
    public Role[] getRoles() {
        return new Role[]{Role.USER, Role.ADMIN};
    }

    @Override
    public void execute() {
        @NotNull final String userId = serviceLocator.getAuthService().getUserId();
        System.out.println("[REMOVE PROJECT]");
        System.out.println("ENTER PROJECT ID:");
        @Nullable final String id = TerminalUtil.nextLine();
        Optional.ofNullable(serviceLocator.getProjectService().removeOneById(id, userId))
                .orElseThrow(ProjectNotFoundException::new);
    }

}
