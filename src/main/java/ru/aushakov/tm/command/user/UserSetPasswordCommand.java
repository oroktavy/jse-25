package ru.aushakov.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.aushakov.tm.command.AbstractUserCommand;
import ru.aushakov.tm.constant.TerminalConst;
import ru.aushakov.tm.enumerated.Role;
import ru.aushakov.tm.exception.entity.UserNotFoundException;
import ru.aushakov.tm.util.TerminalUtil;

import java.util.Optional;

public final class UserSetPasswordCommand extends AbstractUserCommand {

    @Override
    @NotNull
    public String getName() {
        return TerminalConst.USER_SET_PASSWORD;
    }

    @Override
    @Nullable
    public String getArgument() {
        return null;
    }

    @Override
    @NotNull
    public String getDescription() {
        return "Set user password";
    }

    @Override
    @NotNull
    public Role[] getRoles() {
        return new Role[]{Role.ADMIN};
    }

    @Override
    public void execute() {
        System.out.println("[SET USER PASSWORD]");
        System.out.println("ENTER LOGIN:");
        @Nullable final String login = TerminalUtil.nextLine();
        System.out.println("ENTER NEW PASSWORD:");
        @Nullable final String newPassword = TerminalUtil.nextLine();
        Optional.ofNullable(serviceLocator.getUserService().setPassword(login, newPassword))
                .orElseThrow(UserNotFoundException::new);
    }

}
