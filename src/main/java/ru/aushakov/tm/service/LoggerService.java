package ru.aushakov.tm.service;

import org.apache.commons.lang3.StringUtils;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.aushakov.tm.api.service.ILoggerService;

import java.io.IOException;
import java.util.Optional;
import java.util.logging.*;

public final class LoggerService implements ILoggerService {

    @NotNull
    private static final String COMMANDS = "COMMANDS";

    @NotNull
    private static final String COMMANDS_FILE = "./commands.txt";

    @NotNull
    private static final String ERRORS = "ERRORS";

    @NotNull
    private static final String ERRORS_FILE = "./errors.txt";

    @NotNull
    private static final String MESSAGES = "MESSAGES";

    @NotNull
    private static final String MESSAGES_FILE = "./messages.txt";

    @NotNull
    private final LogManager manager = LogManager.getLogManager();

    @NotNull
    private final Logger root = Logger.getLogger("");

    @NotNull
    private final Logger commands = Logger.getLogger(COMMANDS);

    @NotNull
    private final Logger errors = Logger.getLogger(ERRORS);

    @NotNull
    private final Logger messages = Logger.getLogger(MESSAGES);

    {
        init();
        register(commands, COMMANDS_FILE, false);
        register(errors, ERRORS_FILE, true);
        register(messages, MESSAGES_FILE, true);
    }

    private void init() {
        try {
            manager.readConfiguration(LoggerService.class.getResourceAsStream("/logger.properties"));
        } catch (@NotNull final IOException e) {
            root.severe(e.getMessage());
        }
    }

    @NotNull
    private ConsoleHandler getConsoleHandler() {
        @NotNull final ConsoleHandler handler = new ConsoleHandler();
        handler.setFormatter(new Formatter() {
            @Override
            public String format(@NotNull LogRecord record) {
                return record.getMessage() + "\n";
            }
        });
        return handler;
    }

    private void register(
            @NotNull final Logger logger,
            @NotNull final String fileName,
            final boolean consoleEnabled
    ) {
        try {
            if (consoleEnabled) logger.addHandler(getConsoleHandler());
            logger.setUseParentHandlers(false);
            logger.addHandler(new FileHandler(fileName));
        } catch (@NotNull final IOException e) {
            root.severe(e.getMessage());
        }
    }

    @Override
    public void info(@Nullable final String message) {
        if (StringUtils.isEmpty(message)) return;
        messages.info(message);
    }

    @Override
    public void command(@Nullable final String message) {
        if (StringUtils.isEmpty(message)) return;
        commands.info(message);
    }

    @Override
    public void error(@Nullable final Exception e) {
        Optional.ofNullable(e).ifPresent(er -> errors.log(Level.SEVERE, er.getMessage(), er));
    }

}
