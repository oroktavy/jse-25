package ru.aushakov.tm.service;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.aushakov.tm.api.service.IPropertyService;
import ru.aushakov.tm.enumerated.ConfigProperty;
import ru.aushakov.tm.exception.empty.EmptyPropertyException;

import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.Optional;
import java.util.Properties;

public final class PropertyService implements IPropertyService {

    @NotNull
    private static final String CONFIG_FILE = "application.properties";

    @NotNull
    private static final Properties PROPERTIES = new Properties();

    @SneakyThrows
    public PropertyService() {
        @Nullable final InputStream inputStream = ClassLoader.getSystemResourceAsStream(CONFIG_FILE);
        Optional.ofNullable(inputStream).orElseThrow(FileNotFoundException::new);
        PROPERTIES.load(inputStream);
        if (PROPERTIES.isEmpty()) throw new EmptyPropertyException();
        inputStream.close();
    }

    @Override
    @NotNull
    public String getProperty(@Nullable final ConfigProperty property) {
        Optional.ofNullable(property).orElseThrow(EmptyPropertyException::new);
        @NotNull final String propertyName = property.getPropertyName();
        if (System.getenv().containsKey(propertyName)) return System.getenv(propertyName);
        if (System.getProperties().containsKey(propertyName)) return System.getProperty(propertyName);
        return PROPERTIES.getProperty(propertyName, property.getDefaultValue());
    }

    @Override
    @NotNull
    @SneakyThrows
    public Integer getIntProperty(@Nullable final ConfigProperty property) {
        Optional.ofNullable(property).orElseThrow(EmptyPropertyException::new);
        @NotNull final String propertyName = property.getPropertyName();
        @Nullable String stringProperty;
        if (System.getenv().containsKey(propertyName)) stringProperty = System.getenv(propertyName);
        if (System.getProperties().containsKey(propertyName)) stringProperty = System.getProperty(propertyName);
            else stringProperty = PROPERTIES.getProperty(propertyName, property.getDefaultValue());
        @NotNull final Integer intProperty = Integer.parseInt(stringProperty);
        return intProperty;
    }

}
