package ru.aushakov.tm.exception.empty;

public class EmptyRoleException extends RuntimeException {

    public EmptyRoleException() {
        super("Empty role encountered in list!");
    }

}
