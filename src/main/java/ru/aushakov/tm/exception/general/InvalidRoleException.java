package ru.aushakov.tm.exception.general;

import org.jetbrains.annotations.Nullable;

public class InvalidRoleException extends RuntimeException {

    public InvalidRoleException() {
        super("Role is invalid!");
    }

    public InvalidRoleException(@Nullable final String roleId) {
        super("The value '" + roleId + "' entered is not a valid role!");
    }

}
