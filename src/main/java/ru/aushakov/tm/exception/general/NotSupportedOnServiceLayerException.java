package ru.aushakov.tm.exception.general;

import org.jetbrains.annotations.NotNull;

public class NotSupportedOnServiceLayerException extends RuntimeException {

    public NotSupportedOnServiceLayerException(@NotNull final String methodName) {
        super("Method " + methodName + " with such parameters is not supported for service layer call!");
    }

}
