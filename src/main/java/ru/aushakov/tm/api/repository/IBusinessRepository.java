package ru.aushakov.tm.api.repository;

import ru.aushakov.tm.enumerated.Status;
import ru.aushakov.tm.model.AbstractBusinessEntity;

import java.util.Comparator;
import java.util.List;

public interface IBusinessRepository<E extends AbstractBusinessEntity> extends IRepository<E> {

    List<E> findAll(String userId);

    List<E> findAll(String userId, Comparator<E> comparator);

    void clear(String userId);

    E findOneById(String id, String userId);

    E findOneByIndex(Integer index, String userId);

    E findOneByName(String name, String userId);

    E removeOneById(String id, String userId);

    E removeOneByIndex(Integer index, String userId);

    E removeOneByName(String name, String userId);

    E updateOneById(String id, String userId, String name, String description);

    E updateOneByIndex(Integer index, String userId, String name, String description);

    E startOneById(String id, String userId);

    E startOneByIndex(Integer index, String userId);

    E startOneByName(String name, String userId);

    E finishOneById(String id, String userId);

    E finishOneByIndex(Integer index, String userId);

    E finishOneByName(String name, String userId);

    E changeOneStatusById(String id, Status status, String userId);

    E changeOneStatusByIndex(Integer index, Status status, String userId);

    E changeOneStatusByName(String name, Status status, String userId);

}
